package app.hw2.model;

import app.hw2.model.ModelException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Database {
	public static Integer maxProducts = 100;
	private static String user = "www";
	private static String password = "jxmAYp44xohxQbiM";
	private static String url = "jdbc:postgresql://127.0.0.1:5432/hw2";

	private static void printSQLError(SQLException e) {
		System.err.format("ERROR: SQL State %s\n\t%s", e.getSQLState(), e.getMessage());
	}

	public static Connection getConnection() throws ModelException {
		try {
			Connection c = DriverManager.getConnection(url, user, password);
			return c;
		} catch (SQLException e) {
			System.err.format("ERROR: SQL State %s\n\t%s", e.getSQLState(), e.getMessage());
			throw new ModelException("Failed to establish database connection");
		}
	}

	public static void createTable() throws ModelException {
		try (Connection c = Database.getConnection()) {
			ResultSet rs = c.getMetaData().getTables(null, null, "products", null);
			if (!rs.next()) {
				String table_creation_command = "CREATE TABLE products (barcode INTEGER PRIMARY KEY, name TEXT, color TEXT, description TEXT)";
				c.prepareStatement(table_creation_command).execute();
				System.out.println("Created 'products' table with:\n\t" + table_creation_command);
			}
		} catch (SQLException e) {
			printSQLError(e);
			throw new ModelException("Failed to create 'products' table");
		}
	}

	private static Product findProduct(Integer barcode) throws ModelException {
		try (Connection c = Database.getConnection()) {
			String product_selection_command = "SELECT * FROM products WHERE barcode = ?";
			PreparedStatement ps = c.prepareStatement(product_selection_command);
			ps.setInt(1, barcode);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				return new Product(barcode, rs.getString(2), rs.getString(3), rs.getString(4));
			} else {
				return null;
			}
		} catch (SQLException e) {
			printSQLError(e);
			throw new ModelException("Failed to find product with barcode " + barcode);
		}
	}

	public static ArrayList<Product> getProducts() throws ModelException {
		try (Connection c = Database.getConnection()) {
			String product_selection_command = "SELECT * FROM products ORDER BY barcode";
			PreparedStatement ps = c.prepareStatement(product_selection_command);
			ResultSet rs = ps.executeQuery();
			ArrayList<Product> products = new ArrayList<Product>();
			while (rs.next()) {
				products.add(new Product(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4)));
			}
			return products;
		} catch (SQLException e) {
			printSQLError(e);
			throw new ModelException("Failed to get products");
		}
	}

	public static Integer getProductCount() throws ModelException {
		try (Connection c = Database.getConnection()) {
			String product_selection_command = "SELECT COUNT(*) FROM products";
			PreparedStatement ps = c.prepareStatement(product_selection_command);
			ResultSet rs = ps.executeQuery();
			rs.next();
			return rs.getInt(1);
		} catch (SQLException e) {
			printSQLError(e);
			throw new ModelException("Failed to get the number of products");
		}
	}

	public static Boolean isFull() throws ModelException {
		return getProductCount() >= maxProducts;
	}

	public static Boolean productAlreadyAdded(Integer barcode) throws ModelException {
		return findProduct(barcode) != null;
	}

	public static void addProduct(Product p) throws ModelException {
		try (Connection c = getConnection()) {
			String product_insertion_command = "INSERT INTO products VALUES(?, ?, ?, ?)";
			PreparedStatement ps = c.prepareStatement(product_insertion_command);
			ps.setInt(1, p.getBarcode());
			ps.setString(2, p.getName());
			ps.setString(3, p.getColor());
			ps.setString(4, p.getDescription());
			ps.execute();
		} catch (SQLException e) {
			printSQLError(e);
			throw new ModelException("Failed to add product " + p.toString());
		}
	}
}