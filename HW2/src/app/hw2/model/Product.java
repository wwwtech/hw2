package app.hw2.model;

public class Product {
	private Integer barcode;
	private String name;
	private String color;
	private String description;

	public Product(Integer barcode, String name, String color, String description) {
		this.barcode = barcode;
		this.name = name;
		this.color = color;
		this.description = description;
	}

	public Integer getBarcode() {
		return barcode;
	}

	public String getName() {
		return name;
	}

	public String getColor() {
		return color;
	}

	public String getDescription() {
		return description;
	}

	public String toString() {
		return String.format("<%d, %s, %s, %s>", barcode, name, color, description);
	}
}