<%!
String emptyFieldValue = "<span class='empty-value'>\u2205</span>";
String parameterToString(HttpServletRequest request, String parameterName){
	String parameterValue = request.getParameter(parameterName).trim();
	return parameterValue.isEmpty() ? emptyFieldValue : parameterValue;
}
%>
<div class="container rows">
	<div class="row">
		<div class="col field-name">Barcode</div>
		<div class="col field-value"><%= parameterToString(request, "product_barcode") %></div>
	</div>
	<div class="row">
		<div class="col field-name">Name</div>
		<div class="col field-value"><%= parameterToString(request, "product_name") %></div>
	</div>
	<div class="row">
		<div class="col field-name">Color</div>
		<div class="col field-value"><%= parameterToString(request, "product_color") %></div>
	</div>
	<div class="row">
		<div class="col field-name">Description</div>
		<div class="col field-value"><%= parameterToString(request, "product_description") %></div>
	</div>
</div>
