<!DOCTYPE html>
<html>
<head>
<%@include file="include/head.html"%>
<%!String title = "Unknown Failure";%>
</head>
<body>
	<h1><%=title%></h1>
	<p>Something went wrong!</p>
	<%@ include file="include/footer.html"%>
</body>
</html>